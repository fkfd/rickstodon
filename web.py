"""
In case you don't want to configure nginx, here's a python server script to get it running
Install `pyramid` and its dependencies
Then, run web.py as a background process
"""
from wsgiref.simple_server import make_server
from pyramid.view import view_config
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from os import path

RICKROLL = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'


@view_config(route_name='index')
def index(req):
    raise HTTPFound(location='/about')


@view_config(route_name='about')
def about(req):
    return Response(open('./about.html').read())


@view_config(route_name='rickroll_1')
def rickroll_1(req):
    # add more usernames if necessary
    if req.matchdict['a'] in ['terms', 'explore', 'public', '@USERNAME']:
        raise HTTPFound(location=RICKROLL)


@view_config(route_name='rickroll_2')
def rickroll_2(req):
    if req.matchdict['a'] == 'about' and req.matchdict['b'] == 'more':
        raise HTTPFound(location=RICKROLL)


@view_config(route_name='rickroll_3')
def rickroll_3(req):
    if (req.matchdict['a'] == 'auth'
        and req.matchdict['b'] == 'password'
            and req.matchdict['c'] == 'new'):
        raise HTTPFound(location=RICKROLL)


if __name__ == '__main__':
    with Configurator() as config:
        config.add_static_view(
            'about_files',
            path.join(path.dirname(__file__), 'about_files'))
        config.add_route('index', '/')
        config.add_route('about', '/about')
        config.add_route('rickroll_1', '/{a}')
        config.add_route('rickroll_2', '/{a}/{b}')
        config.add_route('rickroll_3', '/{a}/{b}/{c}')
        config.scan()
        app = config.make_wsgi_app()

    server = make_server('0.0.0.0', 3904, app)
    print('Server started')
    server.serve_forever()
